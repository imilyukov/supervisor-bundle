<?php

namespace IMilyukov\SupervisorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package IMilyukov\SupervisorBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();

        $rootNode = $builder->root('im_supervisor');
        $rootNode
            ->children()

                ->arrayNode('unix_http_server')
                    ->children()
                        ->scalarNode('file')->end()
                        ->scalarNode('chmod')
                            ->defaultValue('0700')
                        ->end()
                        ->scalarNode('chown')->end()
                        ->scalarNode('username')->end()
                        ->scalarNode('password')->end()
                    ->end()
                ->end()

                ->arrayNode('inet_http_server')
                    ->children()
                        ->scalarNode('port')->isRequired()->end()
                        ->scalarNode('username')->end()
                        ->scalarNode('password')->end()
                    ->end()
                ->end()

                ->arrayNode('supervisord')
                    ->children()
                        ->scalarNode('logfile')
                            ->defaultValue('$CWD/supervisord.log')
                        ->end()
                        ->scalarNode('logfile_maxbytes')
                            ->defaultValue('50MB')
                        ->end()
                        ->integerNode('logfile_backups')
                            ->defaultValue(10)
                        ->end()
                        ->scalarNode('loglevel')
                            ->defaultValue('info')
                            ->validate()
                            ->ifNotInArray(array('critical', 'error', 'warn', 'info', 'debug', 'trace', 'blather'))
                                ->thenInvalid('Invalid loglevel "%s"')
                            ->end()
                        ->end()
                        ->scalarNode('pidfile')
                            ->defaultValue('$CWD/supervisord.pid')
                        ->end()
                        ->scalarNode('umask')
                            ->defaultValue('022')
                        ->end()
                        ->scalarNode('nodaemon')
                            ->defaultValue('false')
                        ->end()
                        ->integerNode('minfds')
                            ->defaultValue(1024)
                        ->end()
                        ->integerNode('minprocs')
                            ->defaultValue(200)
                        ->end()
                        ->scalarNode('nocleanup')
                            ->defaultValue('false')
                        ->end()
                        ->scalarNode('childlogdir')->end()
                        ->scalarNode('user')->end()
                        ->scalarNode('directory')->end()
                        ->scalarNode('strip_ansi')
                            ->defaultValue('false')
                        ->end()
                        ->scalarNode('environment')->end()
                        ->scalarNode('identifier')
                            ->defaultValue('supervisor')
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('supervisorctl')
                    ->children()
                        ->scalarNode('serverurl')
                            ->defaultValue('http://localhost:9001')
                        ->end()
                        ->scalarNode('username')->end()
                        ->scalarNode('password')->end()
                        ->scalarNode('prompt')
                            ->defaultValue('supervisor')
                        ->end()
                        ->scalarNode('history_file')->end()
                    ->end()
                ->end()

                ->arrayNode('program')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('command')->isRequired()->end()
                            ->scalarNode('process_name')
                                ->defaultValue('%(program_name)s')
                            ->end()
                            ->integerNode('numprocs')
                                ->defaultValue(1)
                            ->end()
                            ->integerNode('numprocs_start')
                                ->defaultValue(0)
                            ->end()
                            ->integerNode('priority')
                                ->defaultValue(999)
                            ->end()
                            ->scalarNode('autostart')
                                ->defaultValue('true')
                            ->end()
                            ->scalarNode('autorestart')
                                ->validate()
                                ->ifNotInArray(array('false', 'unexpected', 'true'))
                                    ->thenInvalid('Invalid autorestart "%s"')
                                ->end()
                            ->end()
                            ->integerNode('startsecs')
                                ->defaultValue(1)
                            ->end()
                            ->integerNode('startretries')
                                ->defaultValue(3)
                            ->end()
                            ->scalarNode('exitcodes')
                                ->defaultValue('0,2')
                            ->end()
                            ->scalarNode('stopsignal')
                                ->defaultValue('TERM')
                                ->validate()
                                ->ifNotInArray(array('TERM', 'HUP', 'INT', 'QUIT', 'KILL', 'USR1', 'USR2'))
                                    ->thenInvalid('Invalid stopsignal "%s"')
                                ->end()
                            ->end()
                            ->integerNode('stopwaitsecs')
                                ->defaultValue(10)
                            ->end()
                            ->scalarNode('stopasgroup')
                                ->defaultValue('false')
                            ->end()
                            ->scalarNode('killasgroup')
                                ->defaultValue('false')
                            ->end()
                            ->scalarNode('user')->end()
                            ->scalarNode('redirect_stderr')
                                ->defaultValue('false')
                            ->end()
                            ->scalarNode('stdout_logfile')
                                ->defaultValue('AUTO')
                            ->end()
                            ->scalarNode('stdout_logfile_maxbytes')
                                ->defaultValue('50MB')
                            ->end()
                            ->integerNode('stdout_logfile_backups')
                                ->defaultValue(10)
                            ->end()
                            ->scalarNode('stdout_capture_maxbytes')
                                ->defaultValue('0')
                            ->end()
                            ->scalarNode('stdout_events_enabled')
                                ->defaultValue('0')
                            ->end()
                            ->scalarNode('stdout_syslog')
                                ->defaultValue('false')
                            ->end()
                            ->scalarNode('stderr_logfile')
                                ->defaultValue('AUTO')
                            ->end()
                            ->scalarNode('stderr_logfile_maxbytes')
                                ->defaultValue('50MB')
                            ->end()
                            ->integerNode('stderr_logfile_backups')
                                ->defaultValue(10)
                            ->end()
                            ->scalarNode('stderr_capture_maxbytes')
                                ->defaultValue('0')
                            ->end()
                            ->scalarNode('stderr_events_enabled')
                                ->defaultValue('false')
                            ->end()
                            ->scalarNode('stderr_syslog')
                                ->defaultValue('false')
                            ->end()
                            ->scalarNode('environment')->end()
                            ->scalarNode('directory')->end()
                            ->scalarNode('umask')
                                ->defaultValue('022')
                            ->end()
                            ->scalarNode('serverurl')
                                ->defaultValue('AUTO')
                            ->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('group')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('programs')->isRequired()->end()
                            ->integerNode('priority')
                                ->defaultValue(999)
                            ->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('rpcinterface')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->prototype('scalar')->end()
                    ->end()
                ->end()

            ->end()
        ;

        return $builder;
    }
}