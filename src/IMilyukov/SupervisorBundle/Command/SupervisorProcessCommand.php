<?php

namespace IMilyukov\SupervisorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Process\Process;

/**
 * Class SupervisorProcessCommand
 * @package IMilyukov\SupervisorBundle\Command
 */
class SupervisorProcessCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('supervisor:process')
            ->setDescription('Supervisor process manager')
            ->addArgument('action', InputArgument::REQUIRED, 'Command For CTL')
            ->addArgument('params', InputArgument::OPTIONAL, 'Programs For CTL command')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supervisorctl = $this->getContainer()->getParameter('supervisor.supervisorctl.path');
        $configuration = sprintf(
            '%s/Resources/supervisor/supervisor_%s.conf',
            $this->getContainer()->getParameter('kernel.root_dir'),
            $this->getContainer()->getParameter('kernel.environment')
        );
        if (!file_exists($configuration)) {

            throw new FileNotFoundException('Supervisor configuration not found');
        }

        $action = $input->getArgument('action');
        $params = $input->getArgument('params');

        $processBuilder = new ProcessBuilder();
        $processBuilder->setPrefix($supervisorctl);
        $processBuilder->setArguments([
            sprintf('--configuration=%s', $configuration),
            $action,
            $params,
        ]);

        $processBuilder->getProcess()
            ->setTimeout(600)
            ->run(function ($type, $buffer) use ($output) {

            $tag = Process::ERR === $type ? 'error' : 'info';
            $output->writeln("<{$tag}>{$buffer}</{$tag}>");
        });
    }
}
