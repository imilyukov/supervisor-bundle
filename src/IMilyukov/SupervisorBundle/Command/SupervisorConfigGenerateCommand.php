<?php

namespace IMilyukov\SupervisorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SupervisorConfigGenerateCommand
 * @package IMilyukov\SupervisorBundle\Command
 */
class SupervisorConfigGenerateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('supervisor:config:generate')
            ->setDescription('Generate supervisor configuration')
            ->addOption('interactive', 'i', InputOption::VALUE_NONE)
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $configuration = sprintf(
            '%s/Resources/supervisor/supervisor_%s.conf',
            $this->getContainer()->getParameter('kernel.root_dir'),
            $this->getContainer()->getParameter('kernel.environment')
        );
        if (!file_exists(dirname($configuration))) {

            mkdir(dirname($configuration), 0777, true);
        }

        $generator = $this->getContainer()->get('im.supervisord.config.generator');
        file_put_contents($configuration, $content = $generator->generate());

        if ($input->getOption('interactive')) {
            $output->write($content);
            $output->write(PHP_EOL);
        }
    }
}
