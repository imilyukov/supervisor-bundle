<?php

namespace IMilyukov\SupervisorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Process\ProcessBuilder;
use Symfony\Component\Process\Process;

/**
 * Class SupervisorSupervisordCommand
 * @package IMilyukov\SupervisorBundle\Command
 */
class SupervisorSupervisordCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('supervisor:supervisord:start')
            ->setDescription('Start Supervisord Command')
            ->addOption('interactive', 'i', InputOption::VALUE_NONE)
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $supervisord   = $this->getContainer()->getParameter('supervisor.supervisord.path');
        $configuration = sprintf(
            '%s/Resources/supervisor/supervisor_%s.conf',
            $this->getContainer()->getParameter('kernel.root_dir'),
            $this->getContainer()->getParameter('kernel.environment')
        );
        if (!file_exists($configuration)) {

            $this->getApplication()->find('supervisor:config:generate')->run($input, $output);
            //throw new FileNotFoundException('Supervisor configuration not found');
        }

        $processBuilder = new ProcessBuilder();
        $processBuilder->setPrefix($supervisord);
        $processBuilder->add(sprintf('--configuration=%s', $configuration));
        if ($input->getOption('interactive')) {

            $processBuilder->add('--nodaemon');
        }

        $processBuilder->getProcess()
            ->setTimeout(0)
            ->run(function ($type, $buffer) use ($output) {

                $tag = Process::ERR === $type ? 'error' : 'info';
                $output->writeln("<{$tag}>{$buffer}</{$tag}>");
            });
    }
}
