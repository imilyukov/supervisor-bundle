<?php

namespace IMilyukov\SupervisorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class IMilyukovSupervisorExtension
 * @package IMilyukov\SupervisorBundle\DependencyInjection
 */
class IMilyukovSupervisorExtension extends Extension
{
    /**
     * @param array $config
     * @param ContainerBuilder $container
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config/'));
        $loader->load('services.yml');

        $processor = new Processor();
        $config = $processor->processConfiguration(new Configuration(), $config);

        // definition resource mapper
        $definition = $container->getDefinition('im.supervisord.config.generator');
        $definition->addMethodCall('setConfiguration', [$config]);

        $container->setDefinition('im.supervisord.config.generator', $definition);
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return 'imilyukov_supervisor';
    }
}
