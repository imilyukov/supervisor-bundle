<?php

namespace IMilyukov\SupervisorBundle;

use IMilyukov\SupervisorBundle\DependencyInjection\IMilyukovSupervisorExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class IMilyukovSupervisorBundle
 * @package IMilyukov\SupervisordBundle
 */
class IMilyukovSupervisorBundle extends Bundle
{
    /**
     * @return bool|ExtensionInterface
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {

            $this->extension = new IMilyukovSupervisorExtension();
        }

        return $this->extension;
    }
}
