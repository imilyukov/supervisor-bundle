<?php

namespace IMilyukov\SupervisorBundle\Config;

/**
 * Class Generator
 * @package IMilyukov\SupervisorBundle\Config
 */
class Generator
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @param array $config
     * @return $this
     */
    public function setConfiguration(array $config)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @return mixed
     */
    public function generate()
    {
        $planeConfig = $this->_planStructure($this->config);

        $output = '';
        foreach($planeConfig as $option) {

            $output .= $this->_generate($option);
        }

        return $this->_normalize($output);
    }

    /**
     * @param $config
     * @param int $level
     * @return string
     */
    protected function _generate($config, $level = 0)
    {
        $output = array('');
        foreach($config as $key => $option) {

            if ( !is_array($option) ) {

                $output[0] = ']' . PHP_EOL;
                $output[] = "{$key}={$option}" . PHP_EOL;

            } else {

                $output[] = !$level ? '[' : ':';
                $output[] = $key;
                $output[] = $this->_generate($option, $level + 1);
            }
        }

        return implode('', $output);
    }

    /**
     * @param $config
     * @return array
     */
    protected function _planStructure($config)
    {
        $planeConfig = array();
        if ( is_array($config) ) {

            foreach($config as $key => $option) {

                if ( is_array($option) ) {

                    foreach($option as $subKey => $subOption) {

                        $planeConfig[] = array(
                            $key => array(
                                $subKey => $this->_planStructure($subOption),
                            ),
                        );
                    }
                } else {

                    $planeConfig[$key] = $option;
                }
            }

            // merge
        } else {

            $planeConfig = $config;
        }

        return $planeConfig;
    }

    protected function _normalize($output)
    {
        // replace banned chars
        $output = str_replace('!#', '.', $output);

        // normalize section
        if ( preg_match_all('/\[(.+)\]([^\[]+)/', $output, $match) ) {

            $sectionNames = $match[1];
            $section = $match[2];

            $normalized = array();
            foreach($sectionNames as $index => $sectionName) {

                $normalized[$sectionName][] = trim($section[$index], PHP_EOL);
            } // foreach

            if ( count($normalized) ) {

                $output = '';
                foreach($normalized as $sectionName => $section) {

                    $output .= PHP_EOL . PHP_EOL . "[{$sectionName}]" . PHP_EOL;
                    $output .= implode(PHP_EOL, $section);
                } // foreach

                $output = trim($output, PHP_EOL);
            } // if
        }

        return $output;
    }
}
